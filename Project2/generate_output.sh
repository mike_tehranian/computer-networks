python run_spanning_tree.py Sample sample_out.txt
python run_spanning_tree.py ComplexLoopTopo complex_loop_topo_out.txt
python run_spanning_tree.py NoLoopTopo no_loop_topo_out.txt
python run_spanning_tree.py SimpleLoopTopo simple_loop_topo_out.txt
python run_spanning_tree.py DanRandom dan_random_out.txt
python run_spanning_tree.py DanRandom1000 dan_random_1000_out.txt
python run_spanning_tree.py DanRandom9 dan_random_9_out.txt
python run_spanning_tree.py DanRandom2000 dan_random_2000_out.txt
python run_spanning_tree.py DanRandom319 dan_random_319_out.txt
python run_spanning_tree.py HardTopo hard_topo_out.txt


python ValidateAnswer.py -s sample_out.txt -r sample_output.txt
python ValidateAnswer.py -s complex_loop_topo_out.txt -r complex_loop_topo_output.txt
python ValidateAnswer.py -s no_loop_topo_out.txt -r no_loop_topo_output.txt
python ValidateAnswer.py -s simple_loop_topo_out.txt -r simple_loop_topo_output.txt
python ValidateAnswer.py -s dan_random_out.txt -r dan_random_output.txt
python ValidateAnswer.py -s dan_random_1000_out.txt -r dan_random_1000_output.txt
python ValidateAnswer.py -s dan_random_9_out.txt -r dan_random_9_output.txt
python ValidateAnswer.py -s dan_random_2000_out.txt -r dan_random_2000_output.txt
python ValidateAnswer.py -s dan_random_319_out.txt -r dan_random_319_output.txt
python ValidateAnswer.py -s hard_topo_out.txt -r hard_topo_output.txt
