# Project 2 for OMS6250
#
# This defines a Switch that can can send and receive spanning tree 
# messages to converge on a final loop free forwarding topology.  This
# class is a child class (specialization) of the StpSwitch class.  To 
# remain within the spirit of the project, the only inherited members
# functions the student is permitted to use are:
#
# self.switchID                   (the ID number of this switch object)
# self.links                      (the list of swtich IDs connected to this switch object)
# self.send_message(Message msg)  (Sends a Message object to another switch)
#
# Student code MUST use the send_message function to implement the algorithm - 
# a non-distributed algorithm will not receive credit.
#
# Student code should NOT access the following members, otherwise they may violate
# the spirit of the project:
#
# topolink (parameter passed to initialization function)
# self.topology (link to the greater topology structure used for message passing)
#
# Copyright 2016 Michael Brown, updated by Kelly Parks
#           Based on prior work by Sean Donovan, 2015


from Message import *
from StpSwitch import *

class Switch(StpSwitch):

    def __init__(self, idNum, topolink, neighbors):    
        # Invoke the super class constructor, which makes available to this object the following members:
        # -self.switchID                   (the ID number of this switch object)
        # -self.links                      (the list of swtich IDs connected to this switch object)
        super(Switch, self).__init__(idNum, topolink, neighbors)
        
        #TODO: Define a data structure to keep track of which links are part of / not part of the spanning tree.
        self.root = idNum
        # Distance to itself is zero
        self.distance_to_root = 0
        # Initially assume that all neighbor links are on the MST
        self.mst_links = set(neighbors)
        self.next_node_to_root = None

    def send_initial_messages(self):
        #TODO: This function needs to create and send the initial messages from this switch.
        #      Messages are sent via the superclass method send_message(Message msg) - see Message.py.
        #      Use self.send_message(msg) to send this.  DO NOT use self.topology.send_message(msg)
        for link in self.links:
            pass_through = link == self.next_node_to_root # is this link my one path to the root?
            new_message = Message(self.root, self.distance_to_root, self.switchID, link, pass_through)
            self.send_message(new_message)

    def process_message(self, message):
        #TODO: This function needs to accept an incoming message and process it accordingly.
        #      This function is called every time the switch receives a new message.

        # The switch will update the root stored in the data structure if the switch receives a message with a root of a lower ID. 
        if message.root < self.root:
            self.root = message.root
            self.distance_to_root = message.distance + 1
            self.mst_links.add(message.origin)
            self.next_node_to_root = message.origin

            # Update neighbors
            self.send_initial_messages()
            return

        # Ignore (incorrect) messages with root IDs greator than my root ID
        if message.root > self.root:
            return

        # If the switch receives a message with pathThrough = True but the switch did not have that originID in its activeLinks list 
        # so the switch must add originID to the activeLinks list or
        if message.pathThrough:
            self.mst_links.add(message.origin)
            return

        # Found a new shorter path to the same root
        if message.distance + 1 < self.distance_to_root:
            self.distance_to_root = message.distance + 1
            self.mst_links.add(message.origin)
            self.next_node_to_root = message.origin
            # Old MST Link is removed on the receiving message with pathThrough==False

            # Update neighbors
            self.send_initial_messages()
        # If the switch receives a message with paththrough = False but the switch 
        # has that originID in its activeLinks so it needs to remove originID from the activeLinks list.
        elif not message.pathThrough and message.origin in self.mst_links and message.distance + 1 > self.distance_to_root:
                self.mst_links.remove(message.origin)
        elif message.distance + 1 == self.distance_to_root:
            # Tiebreaker - use the link with the lower ID
            if message.origin < self.next_node_to_root:
                self.mst_links.remove(self.next_node_to_root)
                self.next_node_to_root = message.origin
                self.mst_links.add(message.origin)
            # Remove link on node with higher ID
            elif message.origin in self.mst_links \
                    and message.origin > self.next_node_to_root:
                self.mst_links.remove(message.origin)
            # Update neighbors
            self.send_initial_messages()

    def generate_logstring(self):
        #TODO: This function needs to return a logstring for this particular switch.  The
        #      string represents the active forwarding links for this switch and is invoked 
        #      only after the simulaton is complete.  Output the links included in the 
        #      spanning tree by increasing destination switch ID on a single line. 
        #      Print links as '(source switch id) - (destination switch id)', separating links 
        #      with a comma - ','.  
        #
        #      For example, given a spanning tree (1 ----- 2 ----- 3), a correct output string 
        #      for switch 2 would have the following text:
        #      2 - 1, 2 - 3
        #      A full example of a valid output file is included (sample_output.txt) with the project skeleton.
        logstring = ""

        for active_link in sorted(self.mst_links):
            logstring += "{} - {}, ".format(self.switchID, active_link)

        return logstring[:-2]
