#!/usr/bin/python
# CS 6250 Spring 2018 - Project 7 - SDN Firewall

from pyretic.lib.corelib import *
from pyretic.lib.std import *
from pyretic.lib.query import packets
from pyretic.core import packet 

def make_firewall_policy(config):
    rules = []

    for entry in config:
        matches = []
        # Config Dict:
        # 'rulenum' 'macaddr_src' 'macaddr_dst' ipaddr_src, 'ipaddr_dst' port_src 'port_dst' protocol
        # Source File:
        # Rule number, srcmac, dstmac, srcip, dstip, srcport, dstport, protocol
        for key, value in entry.iteritems():
            new_match = None
            if key == "rulenum":
                # No match rules needed
                continue
            elif value == "-":
                # Treat - in a parsed configuration file field as if you were to ignore it.
                continue
            elif key == "macaddr_src":
                new_match = match(srcmac=EthAddr(value))
            elif key == "macaddr_dst":
                new_match = match(dstmac=EthAddr(value))
            elif key == "ipaddr_src":
                new_match = match(srcip=IPAddr(value))
            elif key == "ipaddr_dst":
                new_match = match(dstip=IPAddr(value))
            elif key == "port_src":
                new_match = match(srcport=int(value))
            elif key == "port_dst":
                new_match = match(dstport=int(value))
            elif key == "protocol":
                if value == "T":
                    new_match = match(protocol=packet.TCP_PROTO)
                elif value == "B":
                    new_match = match(protocol=packet.TCP_PROTO) | match(protocol=packet.UDP_PROTO)
                elif value == "U":
                    new_match = match(protocol=packet.UDP_PROTO)
                elif value == "I":
                    new_match = match(protocol=packet.ICMP_PROTO)

            matches.append(new_match)

        # Only IPV4 will be used in this project
        rule = match(ethtype=packet.IPV4)
        for match_i in matches:
            rule = rule & match_i

        rules.append(rule)


    allowed = ~(union(rules))

    return allowed
