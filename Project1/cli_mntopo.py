from mininet.topo import Topo
from mininet.net import Mininet
from mininet.link import TCLink
from mininet.util import custom
from mininet.log import lg, output, setLogLevel
from mininet.node import CPULimitedHost
from mininet.cli import CLI
import argparse
import sys
import os


# Topology to be instantiated in Mininet
class MNTopo(Topo):
    "Mininet test topology"

    def __init__(self, cpu=.1, max_queue_size=None, **params):

        # Initialize topo
        Topo.__init__(self, **params)

        # Host and link configuration
        hostConfig = {'cpu': cpu}
        linkConfig = {'bw': 50, 'delay': '10ms', 'loss': 0,
                   'max_queue_size': max_queue_size}

        # Hosts and switches
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        sender = self.addHost('sender', **hostConfig)
        receiver = self.addHost('receiver', **hostConfig)

        # Wire receiver
        self.addLink(receiver, s1, **linkConfig)

        # Wire switches
        self.addLink(s1, s2, **linkConfig)
        self.addLink(s2, s3, **linkConfig)

        # Wire sender
        self.addLink(sender, s3, **linkConfig)


def main():
    "Create specified topology and launch the command line interface"    
    topo = MNTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()

    #TODO: Since this topology contains a cycle, we must enable the Spanning Tree Protocol (STP) on each switch.
    #      This is done with the following line of code: s1.cmd('ovs-vsctl set bridge s1 stp-enable=true')
    #      Here, you will need to generate this line of code for each switch.
    #HINT: You will need to get the switch objects from the net object defined above.

#    for i, switch in enumerate(net.switches):
#        switch.cmd('ovs-vsctl set bridge s%s stp-enable=true' % (i + 1))

    CLI(net)
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    main()

