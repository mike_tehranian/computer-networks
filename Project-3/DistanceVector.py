# Project 3 for CS 6250: Computer Networks
#
# This defines a DistanceVector (specialization of the Node class)
# that can run the Bellman-Ford algorithm. The TODOs are all related 
# to implementing BF. Students should modify this file as necessary,
# guided by the TODO comments and the assignment instructions. This 
# is the only file that needs to be modified to complete the project.
#
# Student code should NOT access the following members, otherwise they may violate
# the spirit of the project:
#
# topolink (parameter passed to initialization function)
# self.topology (link to the greater topology structure used for message passing)
#
# Copyright 2017 Michael D. Brown
# Based on prior work by Dave Lillethun, Sean Donovan, and Jeffrey Randow.

from Node import *
from helpers import *

class DistanceVector(Node):

    def __init__(self, name, topolink, outgoing_links, incoming_links):
        ''' Constructor. This is run once when the DistanceVector object is
        created at the beginning of the simulation. Initializing data structure(s)
        specific to a DV node is done here.'''

        super(DistanceVector, self).__init__(name, topolink, outgoing_links, incoming_links)

        #TODO: Create any necessary data structure(s) to contain the Node's internal state / distance vector data
        self.distances = {name: 0}

    def send_initial_messages(self):
        ''' This is run once at the beginning of the simulation, after all
        DistanceVector objects are created and their links to each other are
        established, but before any of the rest of the simulation begins. You
        can have nodes send out their initial DV advertisements here.

        Remember that links points to a list of Neighbor data structure.  Access
        the elements with .name or .weight '''

        # TODO - Each node needs to build a message and send it to each of its neighbors
        # HINT: Take a look at the skeleton methods provided for you in Node.py
        new_message = (self.name, self.distances.items())

        for destination in self.neighbor_names:
            self.send_msg(new_message, destination)

    def process_BF(self):
        ''' This is run continuously (repeatedly) during the simulation. DV
        messages from other nodes are received here, processed, and any new DV
        messages that need to be sent to other nodes as a result are sent. '''

        # Implement the Bellman-Ford algorithm here.  It must accomplish two tasks below:
        # TODO 1. Process queued messages
        distance_update = False
        for msg in self.messages:
            msg_sender_name, msg_distances = msg[:2]
            msg_sender_distance = int(self.get_outgoing_neighbor_weight(msg_sender_name))

            for sender_neighbor_name, distance_from_sender in msg_distances:
                # Assume for this project that nodes will not advertise the distance to itself as any value less than zero.
                if sender_neighbor_name == self.name \
                    or (sender_neighbor_name in self.distances \
                            and self.distances[sender_neighbor_name] == -99):
                    continue

                if distance_from_sender == -99:
                    # Further, a Node seeing an advertised vector of -99 from a
                    # downstream neighbor can assume this means it can reach that
                    # same destination at infinitely low cost (-99).
                    calculated_distance = -99
                else:
                    calculated_distance = msg_sender_distance + distance_from_sender

                if sender_neighbor_name not in self.distances:
                    self.distances[sender_neighbor_name] = calculated_distance
                    distance_update = True
                elif calculated_distance < -99:
                    # Any node that can reach a destination node and infinitely traverse a negative cycle en
                    # route will set the distance to that node to -99.
                    self.distances[sender_neighbor_name] = -99
                    distance_update = True
                elif calculated_distance < self.distances[sender_neighbor_name]:
                    self.distances[sender_neighbor_name] = calculated_distance
                    distance_update = True

        # Empty queue
        self.messages = []

        # TODO 2. Send neighbors updated distances
        if distance_update:
            self.send_initial_messages()

    def log_distances(self):
        ''' This function is called immedately after process_BF each round.  It
        prints distances to the console and the log file in the following format (no whitespace either end):

        A:A0,B1,C2

        Where:
        A is the node currently doing the logging (self),
        B and C are neighbors, with vector weights 1 and 2 respectively
        NOTE: A0 shows that the distance to self is 0 '''
        
        # TODO: Use the provided helper function add_entry() to accomplish this task (see helpers.py).
        # An example call that which prints the format example text above (hardcoded) is provided.
        #add_entry("A", "A0,B1,C2")

        log_string = ""

        for neighbor, cost in self.distances.iteritems():
            log_string += "{}{},".format(neighbor, cost)

        add_entry(self.name, log_string[:-1])
