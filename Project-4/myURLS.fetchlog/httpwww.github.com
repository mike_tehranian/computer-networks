[1049:1056:3991944335:ERROR:nss_util.cc(90)] Failed to create /home/mininet/.pki/nssdb directory.
[1049:1056:3991945935:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
<stats>
c:tfo.supported:	0
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	1046
c:URLRequestCount:	1
c:disk_cache.miss:	2
c:HttpNetworkTransaction.Count:	2
c:tcp.connect:	5
c:socket.backup_created:	1
c:tcp.write_bytes:	805
c:tcp.read_bytes:	109
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
   46.725 |   297.678 |   250.953 |   0 | www.github.com:80 ->  192.30.253.113:80 192.30.253.112:80
   46.750 |    46.750 |     0.000 |   1 | www.github.com:80 ->  nil
  297.655 |   297.655 |     0.000 |   1 | www.github.com:80 ->  nil
  477.998 |   877.760 |   399.762 |   0 | www.github.com:443 ->  192.30.253.113:443 192.30.253.112:443
  478.103 |   478.103 |     0.000 |   1 | www.github.com:443 ->  nil
  478.120 |   478.120 |     0.000 |   1 | www.github.com:443 ->  nil
  699.040 |   699.040 |     0.000 |   1 | www.github.com:443 ->  nil
  699.126 |   699.126 |     0.000 |   1 | www.github.com:443 ->  nil
  699.135 |   699.135 |     0.000 |   1 | www.github.com:443 ->  nil
  877.695 |   877.695 |     0.000 |   1 | www.github.com:443 ->  nil
  877.752 |   877.752 |     0.000 |   1 | www.github.com:443 ->  nil
  877.759 |   877.759 |     0.000 |   1 | www.github.com:443 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
   46.695 |   477.434 |   430.739 | http://www.github.com/
  477.958 |  1064.532 |   586.574 | https://www.github.com/
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
Moved Permanently |                 |         | http://www.github.com/ -> https://www.github.com/
             |                 |         | https://www.github.com/ -> 
</responses>

<queries>
Collections of histograms for DNS.
Histogram: AsyncDNS.HaveDnsConfig recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.AttemptDiscarded recorded 1 samples, average = 1.0 (flags = 0x1)
0  O                                                                         (0 = 0.0%)
1  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
2  ... 

Histogram: DNS.AttemptSuccess recorded 1 samples, average = 1.0 (flags = 0x1)
0  O                                                                         (0 = 0.0%)
1  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
2  ... 

Histogram: DNS.AttemptSuccessDuration recorded 1 samples, average = 64.0 (flags = 0x1)
0   ... 
58  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
67  ... 

Histogram: DNS.AttemptTimeSavedByRetry recorded 1 samples, average = 3992018.0 (flags = 0x1)
0        ... 
3600000  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}

Histogram: DNS.JobQueueTime recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.JobQueueTimeAfterChange recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.JobQueueTimeAfterChange_LOWEST recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.JobQueueTime_LOWEST recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.ResolveCategory recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: DNS.ResolveSuccess recorded 1 samples, average = 63.0 (flags = 0x1)
0   ... 
58  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
67  ... 

Histogram: DNS.ResolveSuccess_FAMILY_UNSPEC recorded 1 samples, average = 63.0 (flags = 0x1)
0   ... 
58  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
67  ... 

Histogram: DNS.TotalTime recorded 5 samples, average = 12.8 (flags = 0x1)
0   ------------------------------------------------------------------------O (4 = 80.0%)
1   ... 
58  ----O                                                                     (1 = 20.0%) {80.0%}
67  ... 


Collections of histograms for Net.
Histogram: Net.ConnectionTypeCount3 recorded 5 samples, average = 1.4 (flags = 0x1)
0  ------------------------------------------------------------------------O (4 = 80.0%)
1  ... 
7  ------------------O                                                       (1 = 20.0%) {80.0%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 4 samples, average = 92.2 (flags = 0x1)
0    ... 
78   ------------------------------------O                                     (1 = 25.0%) {0.0%}
88   ------------------------------------------------------------------------O (2 = 50.0%) {25.0%}
100  ------------------------------------O                                     (1 = 25.0%) {75.0%}
113  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 1 samples, average = 338.0 (flags = 0x1)
0    ... 
307  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
348  ... 

Histogram: Net.HttpJob.TotalTime recorded 2 samples, average = 508.0 (flags = 0x1)
0    ... 
378  ------------------------------------------------------------------------O (1 = 50.0%) {0.0%}
449  O                                                                         (0 = 0.0%) {50.0%}
533  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
633  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 2 samples, average = 508.0 (flags = 0x1)
0    ... 
378  ------------------------------------------------------------------------O (1 = 50.0%) {0.0%}
449  O                                                                         (0 = 0.0%) {50.0%}
533  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
633  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 1 samples, average = 430.0 (flags = 0x1)
0    ... 
378  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
449  ... 

Histogram: Net.HttpResponseCode recorded 1 samples, average = 301.0 (flags = 0x1)
0    ... 
301  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
302  ... 

Histogram: Net.HttpSocketType recorded 1 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 2 samples, average = 507.5 (flags = 0x1)
0    ... 
389  ------------------------------------------------------------------------O (1 = 50.0%) {0.0%}
477  O                                                                         (0 = 0.0%) {50.0%}
585  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
718  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 3 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
3  ... 

Histogram: Net.Priority_High_Latency_b recorded 1 samples, average = 427.0 (flags = 0x1)
0    ... 
394  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
446  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 113.0 (flags = 0x1)
0    ... 
113  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
114  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 4 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (4 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 4 samples, average = 155.2 (flags = 0x1)
0    ... 
88   ------------------------------------------------------------------------O (2 = 50.0%) {0.0%}
100  ------------------------------------O                                     (1 = 25.0%) {50.0%}
113  ... 
307  ------------------------------------O                                     (1 = 25.0%) {75.0%}
348  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 4 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (4 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 4 samples, average = 92.2 (flags = 0x1)
0    ... 
78   ------------------------------------O                                     (1 = 25.0%) {0.0%}
88   ------------------------------------------------------------------------O (2 = 50.0%) {25.0%}
100  ------------------------------------O                                     (1 = 25.0%) {75.0%}
113  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 4 samples, average = 92.2 (flags = 0x1)
0    ... 
78   ------------------------------------O                                     (1 = 25.0%) {0.0%}
88   ------------------------------------------------------------------------O (2 = 50.0%) {25.0%}
100  ------------------------------------O                                     (1 = 25.0%) {75.0%}
113  ... 

Histogram: Net.Transaction_Connected recorded 1 samples, average = 427.0 (flags = 0x1)
0    ... 
394  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
446  ... 

Histogram: Net.Transaction_Connected_New_b recorded 1 samples, average = 427.0 (flags = 0x1)
0    ... 
394  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
446  ... 


</queries>
